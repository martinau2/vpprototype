﻿Shader "Unlit/PerfectParticleShader"
{
    Properties
    {
		[Header(Texture)]
        _MainTex ("Texture", 2D) = "white" {}
		_TextureDirection("Texture Direction",vector) = (0,0,0,0)
		[HDR]_Color("Color",color) = (1.0,1.0,1.0,1.0)
        _AlphaMask ("Alpha Mask", 2D) = "white" {}
		_AlphaMaskStrenght("Mask Strenght",Range(0,1)) = 1
		[Space(5)]

		[Header(Noise)]
		_Noise("Noise Map",2D) = "black" {}
		_NoiseStrenght("Noise Strenght",Float) = 1
		_NoiseDirection("Noise Direction",vector) = (0,0,0,0)
		[Space(5)]

		[Header(Outline)]
		_Outline("Outline Width",Range(0,1)) = 0

		[Header(Edge)]
		[HDR]_EdgeColor("EdgeColor",color) = (1.0,1.0,1.0,1.0)
		_EdgeWidth("EdgeWidth",float) = 1.0
		[IntRange]_Edge("Edge",Range(0,1)) = 0
		[Space(5)]


		[Header(Displacement)]
		_Displacement("Displacement Map",2D) = "black" {}
		_DisplacementSpeed("Displacement Direction",vector) = (0,0,0,0)
		_DisplacementAmount("Displacement Strenght",Float) = 0
		[Space(5)]


		[Header(Blend State)]
		[Enum(UnityEngine.Rendering.BlendMode)] _SrcBlends("SrcBlend", Float) = 1 //"One"
		[Enum(UnityEngine.Rendering.BlendMode)] _DstBlends("DestBlend", Float) = 1 //"Zero"
		[Space(5)]



		[Header(Other)]
		[Enum(UnityEngine.Rendering.CullMode)] _Culls("Cull", Float) = 0 //"Off"
		[Enum(UnityEngine.Rendering.CompareFunction)] _ZTests("ZTest", Float) = 4 //"LessEqual"
		[Enum(Off,0,On,1)] _ZWrites("ZWrite", Float) = 0.0 //"Off"
		[Enum(UnityEngine.Rendering.ColorWriteMask)] _ColorWriteMasks("ColorWriteMask", Float) = 15 //"All"
    }
    SubShader
    {
        Tags {"Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent"}
		Blend[_SrcBlends][_DstBlends]
		ZTest[_ZTests]
		ZWrite[_ZWrites]
		Cull[_Culls]
		ColorMask[_ColorWriteMasks]

        Pass
        {
            CGPROGRAM
			#pragma target 3.0
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 uv : TEXCOORD0;
                float4 EdgeColorIn : TEXCOORD1;
				float4 color : COLOR;
				float4 normal : NORMAL;
            };

            struct v2f
            {
                float4 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float4 color : COLOR;
				float depth : DEPTH;
				float4 EdgeColorIn : TEXCOORD1;
            };

			sampler2D _CameraDepthNormalsTexture;
            sampler2D _MainTex , _AlphaMask , _Noise , _Displacement;
            float4 _MainTex_ST, _MainTex_TexelSize, _Noise_ST , _Displacement_ST, _Color, _EdgeColor, _NoiseDirection, _TextureDirection, _DisplacementSpeed;
			float   _AlphaMaskStrenght , _NoiseStrenght , _DisplacementAmount , _EdgeWidth , _Outline;
			int _Edge;

            v2f vert (appdata v)
            {
                v2f o;

				o.EdgeColorIn = v.EdgeColorIn;

				float timer = _Time.y / 10;

				float4 timeduv = v.uv;

				timeduv.x += _DisplacementSpeed.x * timer;
				timeduv.y += _DisplacementSpeed.y * timer;

				timeduv.xy *= _Displacement_ST.xy;

				v.vertex += _DisplacementAmount * v.normal  * (((tex2Dlod(_Displacement, timeduv))*2)-1);


                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv.xy = TRANSFORM_TEX(v.uv, _MainTex);
				o.uv.zw = v.uv.zw;
				//Custom Vertex Data;
				//End
                UNITY_TRANSFER_FOG(o,o.vertex);
				o.color = v.color;

				o.depth = -mul(UNITY_MATRIX_MV, v.vertex).z *_ProjectionParams.w;


                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                


				float timer = _Time.y/10;
				float2 NoiseUV = i.uv.xy;
				NoiseUV += timer *  _NoiseDirection;


				float2 NoiseUV2 = i.uv.xy;
				NoiseUV2 += timer * -_NoiseDirection;


				fixed2 distortion = tex2D(_Noise, (NoiseUV*_Noise_ST)).xy ;
				fixed2 distortion2 = tex2D(_Noise, (NoiseUV2*_Noise_ST)).xy;

				distortion = ((distortion*2)-1);
				distortion2 = ((distortion2*2)-1);


				fixed2 finalNoise = (distortion + distortion2)/2;


				fixed2 revealNoise = tex2D(_Noise, i.uv.xy+finalNoise).xy;

				revealNoise = ((revealNoise*2)-1)*_NoiseStrenght;
				








				fixed4 alphamask = tex2D(_AlphaMask, i.uv + revealNoise) * _AlphaMaskStrenght; //Sample alpha mask

                fixed4 col = tex2D(_MainTex, (i.uv.xy + (_TextureDirection*timer) + revealNoise)) ; //Sample color

				float cutout = i.uv.w * _Edge;

				if (cutout < 1.0 && cutout > 0.0)
				{

					if (distortion.x < cutout)
					{
						col = col * 0.0;
					}

					if (distortion.x >= cutout && distortion.x <= cutout + _EdgeWidth)
					{
						col = _EdgeColor;


					}

				}

				

				float max = _Outline;
				float min = clamp(_Outline - _EdgeWidth + _MainTex_TexelSize.x, 0, 1);

				col *= _Color;

				col *= (i.color);

				col *= _Color.a; //Make alpha work

				if (col.x < max && col.x > min)
				{
					col = _EdgeColor * (1.0 - i.uv.z);
				}



				col *= i.color.a;


				return col  * alphamask ;
            }
            ENDCG
        }
    }
}
