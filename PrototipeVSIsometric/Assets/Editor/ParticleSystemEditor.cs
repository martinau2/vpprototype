using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Enemy), true)]
[InitializeOnLoad]
public class ParticleSystemEditor : Editor
{
    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        var tar = target as ParticleSystemEditor;
        GUIStyle StyleofText = new GUIStyle(EditorStyles.label);

        DrawPropertiesExcluding(serializedObject, "");








        serializedObject.ApplyModifiedProperties();


    }


    [DrawGizmo(GizmoType.InSelectionHierarchy | GizmoType.NotInSelectionHierarchy)]
    static void DrawHandles(ParticleManager ps, GizmoType gizmoType)
    {
        if (StaticMetersForProyect.ShowParticleDebugInfo)
        {
            var color = new Color(1, 0.0f, 0.0f, 1);
            Handles.color = color;
            GUI.color = color;
            foreach (var Particle in ps.ParticlesPlaying)
            {
                Handles.Label(Particle.Value.transform.position + Particle.Value.transform.up, Particle.Value.name);
            }
                
           
        }

    }
}


