using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(Player))]
public class PlayerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        var tar = target as Player;
        GUIStyle StyleofText = new GUIStyle(EditorStyles.label);

        DrawPropertiesExcluding(serializedObject, "CurrentStats");

        StyleofText.normal.textColor = new Color(0.99f, 0.7f, 0, 1);
        StyleofText.fontStyle = FontStyle.Bold;

        PlayerStats S = tar.CurrentStats;

        GUILayout.BeginHorizontal();
        GUILayout.Label("Damage Multiplayer:");
        GUILayout.Label(S.DamageMultiplayer * 100 + "%", StyleofText);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Defense:");
        GUILayout.Label(S.Defense + " Flat", StyleofText);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Gold Multiplayer:");
        GUILayout.Label(S.GoldMultiplayer * 100 + "%", StyleofText);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("HP Regen:");
        GUILayout.Label(S.HPRegen + " Flat", StyleofText);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Luck:");
        GUILayout.Label(S.LuckPercent * 100 + "%", StyleofText);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Max HP:");
        GUILayout.Label(S.MaxHPPercent * 100 + "%", StyleofText);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Pickup Range:");
        GUILayout.Label(S.PickupRangePercent * 100 + "%", StyleofText);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Speed:");
        GUILayout.Label(S.SpeedPercent * 100 + "%", StyleofText);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("XP Multiplayer:");
        GUILayout.Label(S.XPMultiplayer * 100 + "%", StyleofText);
        GUILayout.EndHorizontal();

        serializedObject.ApplyModifiedProperties();
    }
}
