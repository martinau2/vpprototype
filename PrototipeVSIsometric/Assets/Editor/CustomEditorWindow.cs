using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

public class CustomEditorWindow : EditorWindow
{
    [MenuItem("Window/My Window")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(CustomEditorWindow));
    }

    void OnGUI()
    {
        GUILayout.Label("Base Settings", EditorStyles.boldLabel);
        //myString = EditorGUILayout.TextField("Text Field", myString);

        //groupEnabled = EditorGUILayout.BeginToggleGroup("Optional Settings", groupEnabled);
        StaticMetersForProyect.ShowEnemyDebugInfo = EditorGUILayout.Toggle("Show Enemy Debug?", StaticMetersForProyect.ShowEnemyDebugInfo);
        StaticMetersForProyect.ShowParticleDebugInfo = EditorGUILayout.Toggle("Show Particles Debug?", StaticMetersForProyect.ShowParticleDebugInfo);
        var pm = FindObjectOfType<ParticleManager>();
        if (pm != null)
        {
            EditorGUILayout.TextField("Particles Active = " + pm.ParticlesPlaying.Count);
        }

        StaticMetersForProyect.WeaponToAdd = EditorGUILayout.ObjectField("Weapon To Add", StaticMetersForProyect.WeaponToAdd, typeof(Weapon), false) as Weapon;
        if(StaticMetersForProyect.WeaponToAdd != null)
        {
            if (GUILayout.Button("AddWeapon"))
            {
                PlayerInventory inv = FindObjectOfType<PlayerInventory>();
                inv.AddWeapon(StaticMetersForProyect.WeaponToAdd);
            }
        }
        
    }
}

public class StaticMetersForProyect
{
    public static bool ShowEnemyDebugInfo = false;
    public static bool ShowParticleDebugInfo = true;
    public static Weapon WeaponToAdd =null;
}
