﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UIElements;


    [CustomEditor(typeof(StateController))]
public class StateControllerEditor : Editor
{
    SerializedProperty stats;

    private void OnEnable()
    {
        stats = serializedObject.FindProperty("_SM");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        var tar = target as StateController;

        tar.statemachinedescription = GUILayout.TextArea(tar.statemachinedescription);
        GUILayout.Label("Current State:");
        if (tar._SM != null)
        {
            if (tar._SM.GetCurrentState() != null)
            {
                tar._CURRENTSTATE = tar._SM.GetCurrentState().ToString();
                GUILayout.Label(tar._CURRENTSTATE);
            }
            else
            {
                GUILayout.Label("No Current State");
            }
        }
        else
        {
            GUILayout.Label("No State Machine");
        }

        serializedObject.ApplyModifiedProperties();

    }
}
