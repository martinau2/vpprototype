using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Enemy), true)]
[InitializeOnLoad]
public class EnemyEditor : Editor
{
    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        var tar = target as Enemy;
        GUIStyle StyleofText = new GUIStyle(EditorStyles.label);

        DrawPropertiesExcluding(serializedObject, "");

       






        serializedObject.ApplyModifiedProperties();


    }
  

    [DrawGizmo(GizmoType.InSelectionHierarchy | GizmoType.NotInSelectionHierarchy)]
    static void DrawHandles(Enemy en, GizmoType gizmoType)
    {
        if (StaticMetersForProyect.ShowEnemyDebugInfo)
        {
            var color = new Color(1, 0.0f, 0.0f, 1);
            Handles.color = color;
            Handles.DrawWireDisc(en.transform.position, en.transform.up, en._RadiusOfAttack);
            GUI.color = color;
            if (en._SC._SM._CurrentState != null)
            {
                Handles.Label(en.transform.position + en.transform.up, en._SC._SM._CurrentState.ToString());
            }
        }
        
    }
}

