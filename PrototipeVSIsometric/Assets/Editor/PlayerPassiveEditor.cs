using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UIElements;


[CustomEditor(typeof(PlayerPassive))]
public class PlayerPassiveEditor : Editor
{

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        var tar = target as PlayerPassive;
        GUIStyle StyleofText = new GUIStyle(EditorStyles.label);

        GUILayout.BeginHorizontal();
        GUILayout.Space(100f);
        GUILayout.Label("Visual Helper for Player Passives");
        GUILayout.EndHorizontal();


        DrawDefaultInspector();

        bool notSameList = false;

        foreach (var item in tar.STATSTOPOST)
        {
            if (!tar.LevelStats.Contains(item))
            {
                notSameList = true;
            }
        }

        if (notSameList)
        {
            GUILayout.BeginHorizontal();
            StyleofText.normal.textColor = new Color(255, 0, 0, 1);
            StyleofText.fontStyle = FontStyle.Bold;
            GUILayout.Space(50f);
            EditorGUILayout.TextField("POSTED NEEDED", StyleofText);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("POST"))
            {
                tar.Post();
            }
            GUILayout.EndHorizontal();
        }
        else
        {

            GUILayout.BeginHorizontal();
            StyleofText.normal.textColor = new Color(0, 255, 0, 1);
            StyleofText.fontStyle = FontStyle.Bold;
            GUILayout.Space(50f);
            EditorGUILayout.TextField("POSTED SUCCESS", StyleofText);
            GUILayout.EndHorizontal();
        }

        GUILayout.Space(50f);

        GUILayout.BeginHorizontal();
        GUILayout.Label("Current Posted Values :");
        GUILayout.EndHorizontal();

        if (tar.Level < tar.LevelStats.Count && tar.Level >= 0)
        {
            StyleofText.normal.textColor = new Color(0.99f, 0.7f, 0, 1);
            StyleofText.fontStyle = FontStyle.Bold;

            PlayerStats S = (PlayerStats)tar.Stats;

            GUILayout.BeginHorizontal();
            GUILayout.Label("Damage Multiplayer:");
            GUILayout.Label(S.DamageMultiplayer * 100 + "%",StyleofText);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Defense:");
            GUILayout.Label(S.Defense + " Flat", StyleofText);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Gold Multiplayer:");
            GUILayout.Label(S.GoldMultiplayer * 100 + "%", StyleofText);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("HP Regen:");
            GUILayout.Label(S.HPRegen + " Flat", StyleofText);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Luck:");
            GUILayout.Label(S.LuckPercent * 100 + "%", StyleofText);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Max HP:");
            GUILayout.Label(S.MaxHPPercent * 100 + "%", StyleofText);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Pickup Range:");
            GUILayout.Label(S.PickupRangePercent * 100 + "%", StyleofText);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Speed:");
            GUILayout.Label(S.SpeedPercent * 100 + "%", StyleofText);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("XP Multiplayer:");
            GUILayout.Label(S.XPMultiplayer * 100 + "%", StyleofText);
            GUILayout.EndHorizontal();
        }
        else
        {
            GUILayout.BeginHorizontal();
            StyleofText.normal.textColor = new Color(255, 0, 0, 1);
            StyleofText.fontStyle = FontStyle.Bold;
            GUILayout.Space(50f);
            EditorGUILayout.TextField("Level outside of bounds of stadistics ", StyleofText);
            GUILayout.EndHorizontal();
        }


    }

}