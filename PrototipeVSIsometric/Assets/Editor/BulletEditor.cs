using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Bullet),true)]
public class BulletEditor : Editor
{
    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        var tar = target as Bullet;
        GUIStyle StyleofText = new GUIStyle(EditorStyles.label);

        DrawPropertiesExcluding(serializedObject, "ModifiedStats");



        BulletStats S = tar.ModifiedStats;

       

        StyleofText.normal.textColor = new Color(0.99f, 0.7f, 0, 1);
        StyleofText.fontStyle = FontStyle.Bold;

        EditorGUILayout.TextField("CURRENT STATS :", StyleofText);

        GUILayout.BeginHorizontal();
        GUILayout.Label("Damage:");
        GUILayout.Label(S.Damage + " Flat", StyleofText);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Size:");
        GUILayout.Label(S.Size + " Flat", StyleofText);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Proyectile Speed:");
        GUILayout.Label(S.ProyectileSpeed + " Flat", StyleofText);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Status Effect:");
        GUILayout.Label(S.StatusEffect + " Flat", StyleofText);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Duration:");
        GUILayout.Label(S.Duration + " Flat", StyleofText);
        GUILayout.EndHorizontal();
        
        
            
        

        serializedObject.ApplyModifiedProperties();


    }
}

