using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UIElements;


[CustomEditor(typeof(WeaponPassive))]
public class WeaponPassiveEditor : Editor
{

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        var tar = target as WeaponPassive;
        GUIStyle StyleofText = new GUIStyle(EditorStyles.label);

        GUILayout.BeginHorizontal();
        GUILayout.Space(100f);
        GUILayout.Label("Visual Helper for Weapons Passives");
        GUILayout.EndHorizontal();


        DrawDefaultInspector();

        bool notSameList = false;

        foreach (var item in tar.STATSTOPOST)
        {
            if (!tar.LevelStats.Contains(item))
            {
                notSameList = true;
            }
        }

        if (notSameList)
        {
            GUILayout.BeginHorizontal();
            StyleofText.normal.textColor = new Color(255, 0, 0, 1);
            StyleofText.fontStyle = FontStyle.Bold;
            GUILayout.Space(50f);
            EditorGUILayout.TextField("POSTED NEEDED", StyleofText);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("POST"))
            {
                tar.Post();
            }
            GUILayout.EndHorizontal();
        }
        else
        {
            
            GUILayout.BeginHorizontal();
            StyleofText.normal.textColor = new Color(0,255,0, 1);
            StyleofText.fontStyle = FontStyle.Bold;
            GUILayout.Space(50f);
            EditorGUILayout.TextField("POSTED SUCCESS",StyleofText);
            GUILayout.EndHorizontal();
        }

        GUILayout.Space(50f);

        GUILayout.BeginHorizontal();
        GUILayout.Label("Current Posted Values :");
        GUILayout.EndHorizontal();

        if (tar.Level < tar.LevelStats.Count && tar.Level >= 0)
        {
            StyleofText.normal.textColor = new Color(0.99f, 0.7f, 0, 1);
            StyleofText.fontStyle = FontStyle.Bold;
            WeaponStats S = (WeaponStats)tar.Stats;

            GUILayout.BeginHorizontal();
            GUILayout.Label("Damage:");
            GUILayout.Label(S.Damage + " Flat", StyleofText);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Size:");
            GUILayout.Label(S.SizePercent*100+"%", StyleofText);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Proyectile Speed:");
            GUILayout.Label(S.ProyectileSpeedPercent * 100 + "%", StyleofText);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Proyectile Amount:");
            GUILayout.Label(S.ProyectileAmount + " Flat", StyleofText);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Status Effect:");
            GUILayout.Label(S.StatusEffectPercent * 100 + "%", StyleofText);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Duration:");
            GUILayout.Label(S.DurationPercent * 100 + "%", StyleofText);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Cooldown:");
            GUILayout.Label(S.CooldownPercent * 100 + "%", StyleofText);
            GUILayout.EndHorizontal();
        }
        else
        {
            GUILayout.BeginHorizontal();
            StyleofText.normal.textColor = new Color(255, 0, 0, 1);
            StyleofText.fontStyle = FontStyle.Bold;
            GUILayout.Space(50f);
            EditorGUILayout.TextField("Level outside of bounds of stadistics ", StyleofText);
            GUILayout.EndHorizontal();
        }
        

    }

}