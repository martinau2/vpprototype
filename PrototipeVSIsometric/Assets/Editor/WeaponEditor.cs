using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Weapon))]
public class WeaponEditor : Editor
{
    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        var tar = target as Weapon;
        GUIStyle StyleofText = new GUIStyle(EditorStyles.label);

        DrawPropertiesExcluding(serializedObject, "CurrentStats");

        

        WeaponStats S = tar.CurrentStats;

        GUILayout.BeginHorizontal();
        StyleofText.normal.textColor = Color.HSVToRGB((float)tar.Level/20,1,1);
        StyleofText.fontStyle = FontStyle.Bold;
        GUILayout.Space(50f);
        EditorGUILayout.TextField("LEVEL " + tar.Level, StyleofText);
        if (GUILayout.Button("+"))
        {
            tar.LevelUp();
        }
        else if (GUILayout.Button("-"))
        {
            tar.LevelDown();
        }
        GUILayout.EndHorizontal();

        StyleofText.normal.textColor = new Color(0.99f, 0.7f, 0, 1);
        StyleofText.fontStyle = FontStyle.Bold;

        GUILayout.BeginHorizontal();
        GUILayout.Label("Damage:");
        GUILayout.Label(S.Damage + " Flat", StyleofText);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Size:");
        GUILayout.Label(S.SizePercent * 100 + "%", StyleofText);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Proyectile Speed:");
        GUILayout.Label(S.ProyectileSpeedPercent * 100 + "%", StyleofText);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Proyectile Amount:");
        GUILayout.Label(S.ProyectileAmount + " Flat", StyleofText);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Status Effect:");
        GUILayout.Label(S.StatusEffectPercent * 100 + "%", StyleofText);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Duration:");
        GUILayout.Label(S.DurationPercent * 100 + "%", StyleofText);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Cooldown:");
        GUILayout.Label(S.CooldownPercent * 100 + "%", StyleofText);
        GUILayout.EndHorizontal();

        serializedObject.ApplyModifiedProperties();
    }
}
