﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[AddComponentMenu("Listeners/Listener", 0)]
public class Listener : MonoBehaviour
{
    [SerializeField]
    [TextArea(order = 25)]
    private string Description;
    public ScriptableEvent Event;
    public UnityEvent Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);

    }
    private void OnDisable()
    {
        Event.DeRegisterListener(this);
    }
    public void OnEventRaised()
    {
        Response.Invoke();
    }
}
