using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "GenericEvent", menuName = "Events/GenericEvent")]
public class ScriptableGenericEvent <T> : ScriptableObject
{
    [SerializeField]
    [TextArea(order = 25)]
    private string Description;
    private List<GenericListener<T>> listeners = new List<GenericListener<T>>();

    Object obj;

    

    public void Raise(T info)
    {

        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised(info);

        }
    }
    public void RegisterListener(GenericListener<T> listener)
    {
        if (!listeners.Contains(listener))
        {
            listeners.Add(listener);
        }
        else { return; }
    }
    public void DeRegisterListener(GenericListener<T> listener)
    {
        if (listeners.Contains(listener))
        {
            listeners.Remove(listener);
        }
        else { return; }
    }


    
}

