using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

public class GenericListener<T> : MonoBehaviour
{
    [SerializeField]
    [TextArea(order = 25)]
    private string Description;
    public ScriptableGenericEvent<T> Event;
    public UnityEvent<T> Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);

    }
    private void OnDisable()
    {
        Event.DeRegisterListener(this);
    }
    public void OnEventRaised(T info)
    {
        Response.Invoke(info);
    }


    
}
