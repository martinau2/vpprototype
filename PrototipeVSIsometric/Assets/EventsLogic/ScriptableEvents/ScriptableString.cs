using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "StringEvent", menuName = "Events/StringEvent")]
public class ScriptableString : ScriptableGenericEvent<string>
{}
