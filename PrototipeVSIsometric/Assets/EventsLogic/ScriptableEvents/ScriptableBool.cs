using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BoolEvent", menuName = "Events/BoolEvent")]
public class ScriptableBool : ScriptableGenericEvent<bool>
{}
