using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Int Event", menuName = "Events/IntEvent")]
public class ScriptableIntEvent : ScriptableGenericEvent<int>
{
}
