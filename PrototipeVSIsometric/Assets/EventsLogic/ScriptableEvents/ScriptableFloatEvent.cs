using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FloatEvent", menuName = "Events/FloatEvent")]
public class ScriptableFloatEvent : ScriptableGenericEvent<float>
{
}
