using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileBullet : Bullet
{
    [Header("REFERENCES")]
    [SerializeField] private Rigidbody _rb;
    [SerializeField] public Character _target;
    public float _SearchRadius = 10;


    [Header("PREDICTION")]
    [SerializeField] private float _maxDistancePredict = 100;
    [SerializeField] private float _minDistancePredict = 5;
    [SerializeField] private float _maxTimePrediction = 5;
    private Vector3 _standardPrediction, _deviatedPrediction;

    [Header("DEVIATION")]
    [SerializeField] private float _deviationAmount = 50;
    [SerializeField] private float _deviationSpeed = 2;

    public LayerMask _ColissionLayer;
    public LayerMask _DamageLayer;

    public SearchForEnemyState _searchState;
    public GoForEnemyState _goForEnemyState;

    bool hitted = false;
    Vector3 originalSize;
    float timer = 0f;

    private void Awake()
    {
        originalSize = transform.localScale;
    }
    private void Start()
    {
        transform.forward = Vector3.up;

        _searchState = new SearchForEnemyState(this, SC._SM) ;
        _goForEnemyState = new GoForEnemyState(this, SC._SM);

        SC._SM.ChangeState(_searchState);

    }

    private void Update()
    {
        timer += Time.deltaTime;
        if(timer > ModifiedStats.Duration)
        {
            //Debug.Log("Run out of timer");
            ReturnToOrb();
        }
    }

    public override void ResetBullet()
    {
        this._target = null;
        transform.forward = Vector3.up;
        transform.localScale = originalSize;
        if (_searchState != null)
            SC._SM.ChangeState(_searchState);
        //Debug.Log("Reseted " + gameObject.name);
    }

    public override void InitBullet(WeaponStats ws)
    {
        base.InitBullet(ws);
        transform.localScale = originalSize * ModifiedStats.Size;
        hitted = false;
        timer = 0f;
        //Debug.Log("Init " + gameObject.name);
    }


    public void FlyStraight()
    {
        _rb.velocity = transform.forward * ModifiedStats.ProyectileSpeed;
    }

    public void GoForEnemy()
    {
        if(_target != null)
        {
            _rb.velocity = transform.forward * ModifiedStats.ProyectileSpeed;

            var leadTimePercentage = Mathf.InverseLerp(_minDistancePredict, _maxDistancePredict, Vector3.Distance(transform.position, _target.transform.position));

            PredictMovement(leadTimePercentage);

            AddDeviation(leadTimePercentage);

            RotateRocket();
        }
    }

    private void PredictMovement(float leadTimePercentage)
    {
        var predictionTime = Mathf.Lerp(0, _maxTimePrediction, leadTimePercentage);

        _standardPrediction = _target._RB.position + _target._RB.velocity * predictionTime;
    }

    private void AddDeviation(float leadTimePercentage)
    {
        var deviation = new Vector3(Mathf.Cos(Time.time * _deviationSpeed), 0, 0);

        var predictionOffset = transform.TransformDirection(deviation) * _deviationAmount * leadTimePercentage;

        _deviatedPrediction = _standardPrediction + predictionOffset;
    }

    private void RotateRocket()
    {
        var heading = _deviatedPrediction - transform.position;

        var rotation = Quaternion.LookRotation(heading);
        _rb.MoveRotation(Quaternion.RotateTowards(transform.rotation, rotation, (ModifiedStats.ProyectileSpeed*30) * Time.deltaTime));
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (hitted)
        {
            return;
        }
        hitted = true;
        if (((1 << collision.gameObject.layer) & _DamageLayer) != 0)
        {
            hitted = true;
            _target = null;
            //Debug.Log("hit " + this.gameObject.name);
            this.ReturnToOrb();
        }
        else if (((1 << collision.gameObject.layer) & _ColissionLayer) != 0)
        {
            hitted = true;
            _target = null;
            this.ReturnToOrb();
        }
        else
        {
            hitted = false;
        }
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, _standardPrediction);
        Gizmos.color = Color.green;
        Gizmos.DrawLine(_standardPrediction, _deviatedPrediction);
    }
}

public class SearchForEnemyState : DefaultState
{
    public MissileBullet MissileBullet;

    public SearchForEnemyState(MissileBullet _missileBullet, StateMachine stateMachine) : base(stateMachine)
    {
        MissileBullet = _missileBullet;
    }

    public override void Enter()
    {


    }

    public override void HandleInput()
    {

    }

    public override void PhysicsExecute()
    {
        MissileBullet.FlyStraight();
    }

    public override void Update()
    {
        var Targets = Physics.OverlapSphere(MissileBullet.transform.position, MissileBullet._SearchRadius, MissileBullet._DamageLayer);

        if (Targets.Length > 0)
        {
            MissileBullet._target = Targets[0].GetComponent<Character>();
            MissileBullet.SC._SM.ChangeState(MissileBullet._goForEnemyState);
        }
    }

    public override void Exit()
    {
    }

}

public class GoForEnemyState : DefaultState
{
    public MissileBullet MissileBullet;

    public GoForEnemyState(MissileBullet _missileBullet, StateMachine stateMachine) : base(stateMachine)
    {
        MissileBullet = _missileBullet;
    }

    public override void Enter()
    {


    }

    public override void HandleInput()
    {

    }

    public override void PhysicsExecute()
    {
        MissileBullet.GoForEnemy();
    }

    public override void Update()
    {
        if(MissileBullet._target == null)
        {
            MissileBullet.SC._SM.ChangeState(MissileBullet._searchState);
        }
    }

    public override void Exit()
    {
    }

}

