using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public StateController SC;
    private Orb _parentOrb;

    public BulletStats BaseStats;
    public BulletStats ModifiedStats;
    


    public Orb Get_parentOrb()
    {
        return _parentOrb;
    }

    public void Set_parentOrb(Orb value)
    {
        _parentOrb = value;
    }

    public virtual void ResetBullet()
    {
        this.transform.rotation = Quaternion.identity;
    }

    public virtual void InitBullet(WeaponStats weaponStats)
    {
        ModifiedStats.Copy(BaseStats);
        ModifiedStats.AddStats(weaponStats);
    }



    public void ReturnToOrb()
    {
        if(this._parentOrb != null)
        {
            this._parentOrb.Pool.Release(this);
        }
    }
}
