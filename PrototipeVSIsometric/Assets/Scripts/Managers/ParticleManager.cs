using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class ParticleManager : MonoBehaviour
{
    int id = 0;
    public Dictionary<int, ParticleSystem> ParticlesPlaying = new Dictionary<int, ParticleSystem>();

    public static ParticleManager Instance { get; private set; }

    public static ParticleManager GetInstance()
    {
        if (Instance == null)
        {
            Instance = Instantiate(new ParticleManager());
        }
        return Instance;
    }

    private void Awake()
    {
        // If there is an instance, and it's not me, delete myself.

        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    public void PlayAt(ScriptableParticleEffect effect,Vector3 pos,Transform parent = null,Quaternion? rotation = null)
    {
        
        ParticleSystem particle = Instantiate(effect.ParticleToPlay, pos, rotation == null ? Quaternion.identity:(Quaternion)rotation, parent);
        var m = particle.main;
        m.loop = false;
        ParticlesPlaying.Add(id, particle);
        if(effect.DurationOfEffect == 0)
        {
            ManageTimespanOfParticle(id, m.duration);
        }
        else
        {
            ManageTimespanOfParticle(id, effect.DurationOfEffect);
        }
        id++;
    }

    public async void ManageTimespanOfParticle(int id,float duration)
    {
        var particle = ParticlesPlaying[id];
        particle.Play();
        await Task.Delay(TimeSpan.FromSeconds(duration));
        var m = particle.main;
        m.loop = false;
        await Task.Delay(TimeSpan.FromSeconds(0.5f));
        ParticlesPlaying.Remove(id);
        Destroy(particle.gameObject);
    }
}
