using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public abstract class Stats
{
    public abstract void AddStats(Stats stats);
}

[System.Serializable]
public class PlayerStats : Stats
{
    public float GoldMultiplayer;
    public float LuckPercent;
    public float PickupRangePercent;
    public float XPMultiplayer;
    public float SpeedPercent;
    public float MaxHPPercent;
    public float HPRegen;
    public float Defense;
    public float DamageMultiplayer;

    public override void AddStats(Stats _addStats)
    {

        if (_addStats.GetType() == typeof(PlayerStats))
        {
            PlayerStats _addOn = (PlayerStats)_addStats;

            GoldMultiplayer += _addOn.GoldMultiplayer;
            LuckPercent += _addOn.LuckPercent;
            PickupRangePercent += _addOn.PickupRangePercent;
            XPMultiplayer += _addOn.XPMultiplayer;
            SpeedPercent += _addOn.SpeedPercent;
            MaxHPPercent += _addOn.MaxHPPercent;
            HPRegen += _addOn.HPRegen;
            Defense += _addOn.Defense;
            DamageMultiplayer += _addOn.DamageMultiplayer;
        }
    }
}

[System.Serializable]
public class WeaponStats : Stats
{
    public float Damage;
    public float SizePercent;
    public float ProyectileSpeedPercent;
    public float ProyectileAmount;
    public float CooldownPercent;
    public float StatusEffectPercent;
    public float DurationPercent;

    public override void AddStats(Stats _addStats)
    {
        if (_addStats.GetType() == typeof(WeaponStats))
        {
            WeaponStats _addOn = (WeaponStats)_addStats;

            Damage += _addOn.Damage;
            SizePercent += _addOn.SizePercent;
            ProyectileSpeedPercent += _addOn.ProyectileSpeedPercent;
            ProyectileAmount += _addOn.ProyectileAmount;
            CooldownPercent += _addOn.CooldownPercent;
            StatusEffectPercent += _addOn.StatusEffectPercent;
            DurationPercent += _addOn.DurationPercent;
        }
    }

    public void ApplyDMGMultiplayer(float mult)
    {
        Damage *= mult;
    }
}

[System.Serializable]
public class BulletStats : Stats
{
    public float Damage;
    public float Size;
    public float ProyectileSpeed;
    public float StatusEffect;
    public float Duration;

    public override void AddStats(Stats _addStats)
    {
        if (_addStats.GetType() == typeof(WeaponStats))
        {
            WeaponStats _addOn = (WeaponStats)_addStats;

            Damage += _addOn.Damage;
            Size *= _addOn.SizePercent;
            ProyectileSpeed *= _addOn.ProyectileSpeedPercent;
            StatusEffect *= _addOn.StatusEffectPercent;
            Duration *= _addOn.DurationPercent;
        }
    }

    public void Copy(BulletStats s)
    {
        Damage = s.Damage;
        Size = s.Size;
        ProyectileSpeed = s.ProyectileSpeed;
        StatusEffect = s.StatusEffect;
        Duration = s.Duration;
    }

}

