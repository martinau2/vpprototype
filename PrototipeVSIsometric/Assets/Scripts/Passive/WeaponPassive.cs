using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Weapon Passive", menuName = "Passives/WeaponPassive")]
public class WeaponPassive : Passive
{
    public List<WeaponStats> STATSTOPOST = new List<WeaponStats>();


    private void OnEnable()
    {
        Post();
    }

    [ContextMenu("Post Values")]
    public void Post()
    {
        LevelStats = new List<Stats>();

        List<Stats> y = new List<Stats>();

        foreach (var item in STATSTOPOST)
        {
            y.Add(item);
        }

        LevelStats = y;
    }
}
