using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Passive :ScriptableObject
{
    public int Level = 0;
    public List<Stats> LevelStats ;
    public Stats Stats { get => LevelStats[Level];}
}



