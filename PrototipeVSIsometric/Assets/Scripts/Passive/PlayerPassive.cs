using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Player Passive", menuName = "Passives/PlayerPassive")]
public class PlayerPassive : Passive
{
    public List<PlayerStats> STATSTOPOST = new List<PlayerStats>();

    private void OnEnable()
    {
        Post();
    }

    [ContextMenu("Post Values")]
    public void Post()
    {
        LevelStats = new List<Stats>();

        List<Stats> y = new List<Stats>();

        foreach (var item in STATSTOPOST)
        {
            y.Add(item);
        }

        LevelStats = y;
    }
}