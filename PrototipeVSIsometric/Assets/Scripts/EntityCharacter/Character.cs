﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(StateController))]
[RequireComponent(typeof(InputsHolder))]
[RequireComponent(typeof(Rigidbody))]
public class Character : MonoBehaviour
{
    public bool _IsPlayer = true;
    public float _Speed;
    public float _RotationSpeed;
    public float _JumpForce;
    public Vector3 _JumpDirection;
    public Transform _Feet;
    public LayerMask _IsGround;

    public Vector3 targetLookAt = new Vector3(0,0,0);


    public InputsHolder _Inputs;
    public bool _Grounded;

    public StateController _SC;

    public Rigidbody _RB;
    
    public GroundedState _GroundedState;
    public AirState _AirState;

    void Start()
    {
        _SC = GetComponent<StateController>();
        _RB = GetComponent<Rigidbody>();
        _Inputs = GetComponent<InputsHolder>();

        _GroundedState = new GroundedState(this, _SC._SM);
        _AirState = new AirState(this,_SC._SM);
        

        _SC._SM.ChangeState(_GroundedState);
    }


    public void Move(float modifier = 1)
    {
        
        var finalspeed = modifier * _Speed;
        var yv = _RB.velocity.y;

        _RB.velocity = transform.forward * finalspeed * _Inputs._VerticalInput;
        _RB.velocity += transform.right * finalspeed * _Inputs._HorizontalInput;

        Vector3 finalvel = new Vector3(_RB.velocity.x, yv, _RB.velocity.z);
        _RB.velocity = finalvel;

        _RB.velocity = Vector3.ClampMagnitude(_RB.velocity, 20);
        

        var magnitud = _RB.velocity.magnitude;
        var fwrd = transform.forward;
        fwrd.y = _RB.velocity.normalized.y; 
        _RB.velocity = _RB.velocity.normalized;
        _RB.velocity = Vector3.Lerp(_RB.velocity.normalized, fwrd, Time.deltaTime*2);
        _RB.velocity = Vector3.Scale(_RB.velocity, new Vector3(magnitud,magnitud,magnitud));
        
    }

    public void LookForward()
    {
        Vector3 result = _Inputs.RawDirection();

        targetLookAt = new Vector3(result.x + transform.position.x , transform.position.y , result.z + transform.position.z );

        if (Vector3.Distance(targetLookAt, transform.position) > 0.2)
        {
            transform.forward = Vector3.Slerp(transform.forward,  targetLookAt - transform.position, 10);
        }
        
    }
    

    public void Jump()
    {

        _RB.AddForce( (_RB.velocity.normalized+Vector3.up) * _JumpForce , ForceMode.Impulse);
        _Grounded = false;

    }
    

    private void Update()
    {
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(_Feet.position, Vector3.down, out hit, 0.1f, _IsGround))
        {
            _Grounded = true;
        }
        else
        {
            _Grounded = false;
        }
    }

}


public class GroundedState : DefaultState
{
    public Character _Char;

    public GroundedState(Character Char, StateMachine stateMachine) : base(stateMachine)
    {
        _Char = Char;
    }

    public override void Enter()
    {
    }

    public override void HandleInput()
    {
        
    }

    public override void PhysicsExecute()
    {
         _Char.Move();
        if (_Char._IsPlayer)
        {
            _Char.LookForward();
        }
        if (_Char._Grounded && _Char._Inputs._Jump)
        {
            _Char._SC._SM.ChangeState(_Char._AirState);
        }
        
    }

    public override void Update()
    {
        
        
    }

    public override void Exit()
    {
    }

}

public class AirState : DefaultState
{
    public Character _Char;

    public AirState(Character Char, StateMachine stateMachine) : base(stateMachine)
    {
        _Char = Char;
    }

    public override void Enter()
    {
        _Char.Jump();
    }

    public override void HandleInput()
    {
        
    }

    public override void PhysicsExecute()
    {
        _Char.Move();
    }

    public override void Update()
    {

        if (_Char._Grounded)
        {
            _Char._SC._SM.ChangeState(_Char._GroundedState);
        }

    }

    public override void Exit()
    {
    }

}

public class DefaultState : IState
{
    public StateMachine stateMachine;

    protected DefaultState(StateMachine stateMachine)
    {
        this.stateMachine = stateMachine;
    }

    public virtual void Enter()
    {

    }

    public virtual void Update()
    {
    }

    public virtual void Exit()
    {
    }

    public virtual void HandleInput()
    {
    }

    public virtual void PhysicsExecute()
    {
    }

    public void ColliderCheck()
    {
    }
}
