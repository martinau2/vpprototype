﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputsHolder : MonoBehaviour
{

    public float _VerticalInput;
    public float _HorizontalInput;
    public bool _Jump;
    bool takeinput = true;
    public bool _TakePlayerInput = false;

    public float cameraYRotation = 45.0f;
    

    // Update is called once per frame
    void Update()
    {
        if (_TakePlayerInput)
        {
            if (_TakePlayerInput)
            {
                cameraYRotation = 45 - transform.localEulerAngles.y;
            }
            PlayerInputs();
        }
    }

    public void takeinputswitch(bool val)
    {
        SetVerticalInput(0);
        SetHorizontalInput(0);
        SetJumpInput(false);
        takeinput = val;
    }

    public void SetVerticalInput(float val)
    {
        if (!takeinput)
            return;
        _VerticalInput = val;
    }
    public void SetHorizontalInput(float val)
    {
        if (!takeinput)
            return;
        _HorizontalInput = val;
    }
    public void SetJumpInput(bool val)
    {
        if (!takeinput)
            return;
        _Jump = val;
    }

    public void PlayerInputs()
    {
        Vector3 toConvert = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        Vector3 realDirection = IsoVectorConvert(toConvert);
        SetVerticalInput(realDirection.z);
        SetHorizontalInput(realDirection.x);
        SetJumpInput(Input.GetButton("Jump"));
    }

    public Vector3 RawDirection()
    {

        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        Vector3 vector = new Vector3(x, 0, y);
        Quaternion rotation = Quaternion.Euler(0, 45, 0);
        Matrix4x4 isoMatrix = Matrix4x4.Rotate(rotation);
        Vector3 result = isoMatrix.MultiplyPoint3x4(vector);
        return result;
    }

    public Vector3 IsoVectorConvert(Vector3 vector)
    {
        Quaternion rotation = Quaternion.Euler(0, cameraYRotation, 0);
        Matrix4x4 isoMatrix = Matrix4x4.Rotate(rotation);
        Vector3 result = isoMatrix.MultiplyPoint3x4(vector);
        return result;
    }
}
