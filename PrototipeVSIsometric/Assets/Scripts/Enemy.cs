using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    public NavMeshAgent Agent;
    public Transform player;
    public InputsHolder input;

    public StateController _SC;

    public AttackState attckState;
    public ChasingState chaseState;
    


    public float _TurningSpeed = 10;
    public float _RadiusOfAttack = 1;
    // Start is called before the first frame update
    void Start()
    {
        Agent.SetDestination(player.position);
        Agent.updatePosition = false;

        attckState = new AttackState(this, _SC._SM);
        chaseState = new ChasingState(this, _SC._SM);
        _SC._SM.ChangeState(chaseState); 
    }

    // Update is called once per frame
    void Update()
    {

    }


    
    public void Chase()
    {

        Agent.SetDestination(player.position);
        input.SetVerticalInput(Mathf.Clamp01(Vector3.Distance(Agent.nextPosition, transform.position)));
        LookAtAgent();
    }

    public void LookAtAgent()
    {
        Vector3 targtLook = Agent.nextPosition - transform.position;
        targtLook.y = transform.position.y+1;
        transform.forward = Vector3.Slerp(transform.forward, targtLook, Time.deltaTime * _TurningSpeed);
    }

    public void LookAtPlayer()
    {
        Vector3 targtLook = player.transform.position - transform.position;
        targtLook.y = transform.position.y+1;
        transform.forward = Vector3.Slerp(transform.forward, targtLook, Time.deltaTime * _TurningSpeed);
    }
}

public class AttackState : DefaultState
{
    public Enemy _En;

    public AttackState(Enemy en, StateMachine stateMachine) : base(stateMachine)
    {
        _En = en;
    }

    public override void Enter()
    {
    }

    public override void HandleInput()
    {

    }

    public override void PhysicsExecute()
    {
        

    }

    public override void Update()
    {
        _En.LookAtPlayer();

        if (Vector3.Distance(_En.transform.position, _En.player.transform.position) > _En._RadiusOfAttack)
        {
            _En._SC._SM.ChangeState(_En.chaseState);
        }

    }

    public override void Exit()
    {
    }

    
}

public class ChasingState : DefaultState
{
    public Enemy _En;

    public ChasingState(Enemy en, StateMachine stateMachine) : base(stateMachine)
    {
        _En = en;
    }

    public override void Enter()
    {
    }

    public override void HandleInput()
    {

    }

    public override void PhysicsExecute()
    {


    }

    public override void Update()
    {

        _En.Chase();

        if (Vector3.Distance(_En.transform.position, _En.player.transform.position) < _En._RadiusOfAttack)
        {
            _En._SC._SM.ChangeState(_En.attckState);
        }

    }

    public override void Exit()
    {
        _En.input.SetVerticalInput(0);
    }

}
