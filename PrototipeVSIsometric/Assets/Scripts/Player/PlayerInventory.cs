using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour
{

    public List<Weapon> GameWeapons = new List<Weapon>();
    public List<Passive> GamePassives = new List<Passive>();
    public List<Passive> AccountPassives = new List<Passive>();
    float _currentGold = 0;

    Player _player;

    public List<Transform> OrbsHolder;

    void Start()
    {
        _player = GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddWeapon(Weapon weapon)
    {
        if (GameWeapons.Contains(weapon))
        {
            Debug.Log("WEAPON LEVEL UP");
            var w = GameWeapons.Find(_weapon => _weapon == weapon);
            w.LevelUp();

            UpdatePasives();
        }
        else if (GameWeapons.Count <= 5)
        {
            Debug.Log("Adding New Weapon");
            var newWeapon = Instantiate(weapon, OrbsHolder[GameWeapons.Count].position, Quaternion.identity, OrbsHolder[GameWeapons.Count]);
            GameWeapons.Add(newWeapon);
            UpdatePasives();
        }
        else
        {
            Debug.LogError("Max weapon count");
        }

        

    }

    public void AddPassive() { }

    public void AddGold() { }

    public void RemoveWeapon() { }

    public void RemovePassive() { }

    public void UpdatePasives()
    {
        List<Stats> fullList = new List<Stats>();

        foreach (var passive in GamePassives)
        {
            fullList.Add(passive.Stats);
        }

        foreach (var passive in AccountPassives)
        {
            fullList.Add(passive.Stats);
        }

        _player.ApplyStats(fullList);

        foreach (var weapon in GameWeapons)
        {
            weapon.ApplyStats(fullList);
            weapon.CurrentStats.Damage *= _player.CurrentStats.DamageMultiplayer;
        }
        
    }

    public void UpdateWeaponPassives(Weapon weapon)
    {
        List<Stats> fullList = new List<Stats>();

        foreach (var passive in GamePassives)
        {
            fullList.Add(passive.Stats);
        }

        foreach (var passive in AccountPassives)
        {
            fullList.Add(passive.Stats);
        }

        
        weapon.ApplyStats(fullList);
        weapon.CurrentStats.Damage *= _player.CurrentStats.DamageMultiplayer;
        
    }
}
