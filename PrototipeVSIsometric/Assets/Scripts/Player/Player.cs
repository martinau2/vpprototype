using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public PlayerStats BaseStats;
    public PlayerStats CurrentStats;
    PlayerInventory _inventory;
    // Start is called before the first frame update
    void Start()
    {
        _inventory = GetComponent<PlayerInventory>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    internal void ApplyStats(List<Stats> StatsList = null)
    {
        CurrentStats = new PlayerStats();
        CurrentStats.AddStats(BaseStats);
        

        if (StatsList != null || StatsList.Count != 0)
        {
            foreach (var Stat in StatsList)
            {
                CurrentStats.AddStats(Stat);
            }
        }
    }
}
