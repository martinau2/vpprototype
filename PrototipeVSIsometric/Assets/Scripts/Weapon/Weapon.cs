using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{

    public int Level = 1;
    public List<WeaponStats> LevelStats;
    public WeaponStats CurrentStats;
    public float InitialCooldown = 20;
    private float modifiedCD;
    public float _currentCooldown { private set; get; } = 0;
    public Orb WeaponOrb;
    

    public StateController SC;

    public SetUpState _setupState { private set; get; }
    public FireState _fireState { private set; get; }
    public CooldownState _cooldownState { private set; get; }

    public List<Stats> LatestListOfStats = null;

    public ScriptableParticleEffect levelUP = null;

    private void OnEnable()
    {
        this.ApplyStats(LatestListOfStats);
    }

    void Start()
    {
        this.ApplyStats(LatestListOfStats);

        _setupState = new SetUpState(this, SC._SM);
        _fireState = new FireState(this, SC._SM);
        _cooldownState = new CooldownState(this, SC._SM);

        SC._SM.ChangeState(_setupState);
    }

    public void SetCD()
    {
        _currentCooldown = modifiedCD;
    }

    public void ReduceCD()
    {
        _currentCooldown -= 1f * Time.deltaTime;
    }

    public void LevelUp()
    {
        if (Level < LevelStats.Count)
        {
            Level++;
            ApplyStats(LatestListOfStats);
            ParticleManager.GetInstance().PlayAt(levelUP, transform.parent.position - transform.parent.up);
        }
        else
        {
            Debug.LogError("Max level alredy achieved");
        }
    }

    public void LevelDown()
    {
        if (Level > 1)
        {
            Level--;
            ApplyStats(LatestListOfStats);
        }
        else
        {
            Debug.LogError("Level is minimun");
        }
    }

    public void ApplyStats(List<Stats> StatsList = null)
    {
        CurrentStats = new WeaponStats();
        //CurrentStats.AddStats(LevelStats[0]);

        for (int i = 0; i < Level; i++)
        {
            CurrentStats.AddStats(LevelStats[i]);
        }

        if (StatsList != null)
        {
            foreach (var Stat in StatsList)
            {
                CurrentStats.AddStats(Stat);
            }
        }

        modifiedCD = InitialCooldown * CurrentStats.CooldownPercent;

        UpdateOrb();
        LatestListOfStats = StatsList;
    }

    

    public void SetupOrb() 
    {
        WeaponOrb.InitializeVFX();
        WeaponOrb.UpdateOrbStats(this.CurrentStats);
    }

    public void ResetOrb() 
    {
    }

    public void UpdateOrb()
    {
        WeaponOrb.UpdateOrbStats(this.CurrentStats);
    }
}

public class SetUpState : DefaultState
{
    public Weapon Weapon;

    public SetUpState(Weapon _weapon, StateMachine stateMachine) : base(stateMachine)
    {
        Weapon = _weapon;
    }

    public override void Enter()
    {
        Weapon.SetupOrb();

        Weapon.SetCD();

        Weapon.SC._SM.ChangeState(Weapon._cooldownState);

    }

    public override void HandleInput()
    {

    }

    public override void PhysicsExecute()
    {

    }

    public override void Update()
    {


    }

    public override void Exit()
    {
    }

}

public class CooldownState : DefaultState
{
    public Weapon Weapon;

    public CooldownState(Weapon _weapon, StateMachine stateMachine) : base(stateMachine)
    {
        Weapon = _weapon;
    }

    public override void Enter()
    {

        Weapon.WeaponOrb.CDVFX();
    }

    public override void HandleInput()
    {

    }

    public override void PhysicsExecute()
    {

    }

    public override void Update()
    {
        if (Weapon._currentCooldown <= 0)
        {
            Weapon.SC._SM.ChangeState(Weapon._fireState);
        }
        else
        {
            Weapon.ReduceCD();
        }
    }

    public override void Exit()
    {
    }

}

public class FireState : DefaultState
{
    public Weapon Weapon;

    public FireState(Weapon _weapon, StateMachine stateMachine) : base(stateMachine)
    {
        Weapon = _weapon;
    }

    public override void Enter()
    {
        Weapon.WeaponOrb.ShootVFX();

        Weapon.WeaponOrb.Fire(0.1f, Weapon.CurrentStats.ProyectileAmount);

        Weapon.SetCD();

        Weapon.SC._SM.ChangeState(Weapon._cooldownState);

    }

    public override void HandleInput()
    {

    }

    public override void PhysicsExecute()
    {

    }

    public override void Update()
    {


    }

    public override void Exit()
    {
    }

}

