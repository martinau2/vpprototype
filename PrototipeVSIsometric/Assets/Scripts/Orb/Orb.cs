using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Rendering;

public class Orb : MonoBehaviour
{
    
    public ParticleSystem OnActive;
    public ParticleSystem OnCooldown;
    public ParticleSystem OnFire;

    public ObjectPool<Bullet> Pool;
    public Bullet BulletType;

    public Transform BulletInit;

    public WeaponStats statsOfWeapon;

    [ColorUsage(true, true)]
    public Color ActiveOrbColor;

    [ColorUsage(true, true)]
    public Color CDOrbColor;

    [ColorUsage(true, true)]
    public Color ShootOrbColor;

    bool changingvfx = false;
    float id = 0;

    Material orbMaterial;

    // Start is called before the first frame update
    void Start()
    {
        Pool = new ObjectPool<Bullet>(OnGet,OnRelease);

        for (int i = 0; i < 10; i++)
        {
            InstantiateBullet();
        }
        orbMaterial = GetComponent<MeshRenderer>().material;
    }

    private void InstantiateBullet()
    {
        Bullet b = Instantiate(BulletType, Vector3.zero, Quaternion.identity, null);
        b.Set_parentOrb(this);
        b.gameObject.name = id++ + " Bullet Missile";
        Pool.Release(b);
    }

    private void OnRelease(Bullet bullet)
    {
        bullet.gameObject.SetActive(false);
        bullet.ResetBullet();
        //Debug.Log("Release " + bullet.gameObject.name);
    }

    private void OnGet(Bullet bullet)
    {
        //Debug.Log("Get " + bullet.gameObject.name);
        bullet.transform.position = BulletInit.position;
        bullet.InitBullet(statsOfWeapon);
        bullet.gameObject.SetActive(true);

    }

    public async void Fire(float delayBetweenShoots,float shotsAmount)
    {
        

        for (int i = 0; i < shotsAmount; i++)
        {
            if (Pool.countInactive == 0)
            {
                InstantiateBullet();
            }

            await Task.Delay(TimeSpan.FromSeconds(delayBetweenShoots));
            //Debug.Log("About to get bullet ");
            Pool.Get();
        }
    }

    public void UpdateOrbStats(WeaponStats stats)
    {
        statsOfWeapon = stats;
    }
    
    public async void InitializeVFX()
    {
        while (changingvfx)
        {
            await Task.Delay(TimeSpan.FromSeconds(0.01f));
        }
        changingvfx = true;
        float initial = 0f;
        while (initial < 1f && orbMaterial != null)
        {
            orbMaterial.SetColor("_EmissiveColor", Color.Lerp(Color.black, ActiveOrbColor, initial));
            initial += 0.2f ;
            await Task.Delay(TimeSpan.FromSeconds(0.05f));
        }
        changingvfx = false;
    }

    public async void CDVFX()
    {
        while (changingvfx)
        {
            await Task.Delay(TimeSpan.FromSeconds(0.01f));
        }
        changingvfx = true;
        float initial = 0f;
        while (initial < 1f && orbMaterial != null)
        {
            orbMaterial.SetColor("_EmissiveColor", Color.Lerp(ActiveOrbColor, CDOrbColor, initial));
            initial += 0.2f;
            await Task.Delay(TimeSpan.FromSeconds(0.05f));
        }
        changingvfx = false;
    }

    public async void ShootVFX()
    {
        while (changingvfx)
        {
            await Task.Delay(TimeSpan.FromSeconds(0.01f));
        }
        changingvfx = true;
        float initial = 0f;
        while (initial < 1f && orbMaterial != null)
        {
            orbMaterial.SetColor("_EmissiveColor", Color.Lerp(CDOrbColor, ShootOrbColor, initial));
            initial += 0.2f;
            await Task.Delay(TimeSpan.FromSeconds(0.05f));
        }
        changingvfx = false;
    }

}
