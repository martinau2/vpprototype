using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Particles", menuName = "Particles/Particle")]
public class ScriptableParticleEffect : ScriptableObject
{
    public ParticleSystem ParticleToPlay = null;
    public float DurationOfEffect = 0;
    public bool Loop = false;
}
