﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateController : MonoBehaviour
{
    public string statemachinedescription;
    public StateMachine _SM;
    public string _CURRENTSTATE;
    bool _Paused = false;


    void Awake()
    {
        _SM = new StateMachine();
    }
    

    void Update()
    {
        if (_Paused) { return; }
        _SM.InputUpdate();
        _SM.Update();
    }

    private void FixedUpdate()
    {
        if (_Paused) { return; }
        _SM.PhysicsUpdate();
    }

    public void PauseStates(bool condition)
    {
        _Paused = condition;
    }
}
